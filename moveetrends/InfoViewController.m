//
//  InfoViewController.m
//  moveetrends
//
//  Created by Sean Kovacs on 9/16/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import "InfoViewController.h"
#import <Social/Social.h>

@interface InfoViewController ()

@end

@implementation InfoViewController

int ratingRound( float f )
{
    return floor((f * 10 ) + 0.5) / 10;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Label setup
    self.ratingsLabel.font = [UIFont fontWithName:@"fontello" size:14.0f];
    self.facebookButton.titleLabel.font = [UIFont fontWithName:@"fontello" size:22.0f];
    [self.facebookButton setTitle:@"" forState:UIControlStateNormal];
    self.twitterButton.titleLabel.font = [UIFont fontWithName:@"fontello" size:22.0f];
    [self.twitterButton setTitle:@"" forState:UIControlStateNormal];
    
    if(self.movie) {
        self.titleLabel.text = self.movie.title;
        self.taglineLabel.text = self.movie.tagline;
        self.descriptionTextView.text = self.movie.description;
        
        float ratingFive = (self.movie.rating.floatValue/100)*5;
        int wholeStars = ratingRound(ratingFive);

        NSMutableString *stars = [NSMutableString string];
        
        for(int i = 0; i < 5; i++){
            if(i < wholeStars) {
                [stars appendString:@""];
            }else {
                [stars appendString:@""];
            }
        }
        self.ratingsLabel.text = stars;
        
        NSMutableString *genres = [NSMutableString string];
        
        for(int i = 0; i < self.movie.genres.count; i++){
            [genres appendString:self.movie.genres[i]];
            if(i < self.movie.genres.count-1) [genres appendString:@"; "];
        }
        self.genresLabel.text = genres;
        
        [self.trailerButton setTitle:self.movie.trailer forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadMovie:(Movie *)movie
{
    self.movie = movie;
}

- (IBAction)trailerTrash:(id)sender
{
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:self.movie.trailer]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.movie.trailer]];
    }
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)facebook:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *share = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [share addURL:[NSURL URLWithString:self.movie.url]];
        [share setInitialText:[NSString stringWithFormat:@"I'm looking forward to seeing %@ soon!",self.movie.title]];
        
        [self presentViewController:share animated:YES completion:nil];
    }else {
        NSString *message = @"Please add a Facebook account to your device before sharing content on Facebook. You can do this from the iOS Settings app.";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alertView show];
    }
}

- (IBAction)twitter:(id)sender
{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *share = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [share addURL:[NSURL URLWithString:self.movie.url]];
        [share setInitialText:[NSString stringWithFormat:@"I'm looking forward to seeing %@ soon!",self.movie.title]];
        
        [self presentViewController:share animated:YES completion:nil];
    }else {
        NSString *message = @"Please add a Twitter account to your device before sharing content on Twitter. You can do this from the iOS Settings app.";
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:message delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alertView show];
    }
}


@end
