//
//  Movie.h
//  moveetrends
//
//  Created by Sean Kovacs on 9/14/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSNumber *rating;
@property (nonatomic,copy) NSString *description;
@property (nonatomic,copy) NSString *tagline;
@property (nonatomic,strong) NSArray *genres;
@property (nonatomic,copy) NSString *image;
@property (nonatomic,copy) NSString *trailer;
@property (nonatomic,copy) NSString *url;

@end
