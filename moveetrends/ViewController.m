//
//  ViewController.m
//  moveetrends
//
//  Created by Sean Kovacs on 9/14/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import "ViewController.h"
#import "Movie.h"
#import "MovieView.h"
#import "InfoViewController.h"

@interface ViewController ()

@property (nonatomic,strong) NSMutableArray *movies;
@property (nonatomic,getter = isRefreshing) BOOL refreshing;


- (void)beginRefresh;
- (void)endRefresh;

- (void)refreshMovies;
- (void)layoutMovies;
- (int)indexForCurrentMovie;

- (NSArray *)fetchMovies;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.activityView.color = [UIColor colorWithRed:26.0f/255.0f green:177.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
    
    [self refreshMovies];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"InfoViewController"]) {
        InfoViewController *infoVC = (InfoViewController *)segue.destinationViewController;
        
        Movie *currentMovie = [self.movies objectAtIndex:[self indexForCurrentMovie]];
        [infoVC loadMovie:currentMovie];
    }
}

- (IBAction)refresh:(id)sender
{
    [self refreshMovies];
}

- (UIColor *)randomColor {
    CGFloat red =  (CGFloat)arc4random() / (CGFloat)RAND_MAX;
    CGFloat blue = (CGFloat)arc4random() / (CGFloat)RAND_MAX;
    CGFloat green = (CGFloat)arc4random() / (CGFloat)RAND_MAX;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

- (void)beginRefresh
{
    self.refreshing = YES;
    
    [self.activityView startAnimating];
}

- (void)endRefresh
{
    self.refreshing = NO;
    
    [self.activityView stopAnimating];
}

- (void)refreshMovies
{
    if(!self.isRefreshing) {
        
        [self beginRefresh];
        
        // Remove existing
        [self.scrollView setContentOffset:CGPointZero animated:NO];
        [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        if(!self.movies) {
            self.movies = [NSMutableArray arrayWithCapacity:10];
        }else {
            [self.movies removeAllObjects];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            NSArray *movies = [self fetchMovies]; // uses sync download; needs to run on sep. thread
            
            NSArray *sortedMovies = [movies sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"ratings.percentage" ascending:NO]]];
            
            if(movies) {
                
                int moviecount = 10; //sortedMovies.count;
                
                for(int i = 0; i < moviecount; i++) {
                    NSDictionary *movieDict = [sortedMovies objectAtIndex:i];
      
                    Movie *movie = [[Movie alloc] init];
                    movie.title = [movieDict valueForKey:@"title"];
                    movie.rating = [movieDict valueForKeyPath:@"ratings.percentage"];
                    movie.description = [movieDict valueForKey:@"overview"];
                    movie.tagline = [movieDict valueForKey:@"tagline"];
                    movie.genres = [movieDict valueForKey:@"genres"];
                    movie.image = [movieDict valueForKey:@"poster"];
                    movie.trailer = [movieDict valueForKey:@"trailer"];
                    movie.url = [movieDict valueForKey:@"url"];
                    
                    NSLog(@"Movie: %@, Rating: %d",movie.title,[movie.rating intValue]);
                    
                    [self.movies addObject:movie];
                }
                
                // update UI on main thread
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self layoutMovies];
                    
                    [self endRefresh];
                });
            }

        });
        
    }
}

- (void)layoutMovies
{
    // Install movieViews
    
    CGRect frame = self.scrollView.bounds;
    
    MovieView *previousView = nil;
    
    for(int i = 0; i < self.movies.count; i++) {
        MovieView *movieView = [[MovieView alloc] initWithFrame:CGRectOffset(frame, self.scrollView.bounds.size.width*i, 0.0f)];
        movieView.translatesAutoresizingMaskIntoConstraints = NO;
        [movieView loadMovie:[self.movies objectAtIndex:i]];
        [self.scrollView addSubview:movieView];

        if(previousView) {
            [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[previousView]-(0)-[movieView(==scrollView)]" options:0 metrics:nil views:@{@"movieView":movieView, @"scrollView":self.scrollView, @"previousView":previousView}]];
            [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[movieView(==scrollView)]" options:0 metrics:nil views:@{@"movieView":movieView, @"scrollView":self.scrollView}]];
            
            if(i == self.movies.count-1) {
                [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[previousView]-(0)-[movieView(==scrollView)]-(0)-|" options:0 metrics:nil views:@{@"movieView":movieView, @"scrollView":self.scrollView, @"previousView":previousView}]];
            }
            
        }else {
            [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[movieView(==scrollView)]" options:0 metrics:nil views:@{@"movieView":movieView, @"scrollView":self.scrollView}]];
            [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[movieView(==scrollView)]" options:0 metrics:nil views:@{@"movieView":movieView, @"scrollView":self.scrollView}]];
        }
        
        previousView = movieView;

    }
}

- (int)indexForCurrentMovie
{
    return self.scrollView.contentOffset.x / self.scrollView.frame.size.width;
}

- (NSArray *)fetchMovies
{
    // Normally I would use AFNetworking for block convenience
    // Since this is a simple app, I'll forgo using the delegate for NSURLConnection and go with the thread blocking sync method...
    // Should be launched on another thread so the UI isn't blocked
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://api.trakt.tv/movies/trending.json/792803e6b3799d3d435b4ba0ed8c3797"]];
    NSError *dataError;
    NSData *jsonData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&dataError];
    
    if(!dataError && jsonData && jsonData.length > 0) {
        
        NSError *jsonError;
        id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&jsonError];
        
        if(!jsonError && jsonObject) {
            
            NSArray *jsonArray = (NSArray *)jsonObject;
            //NSLog(@"Dict dump: %@",jsonArray.description);
            
            return jsonArray;
            
        }else {
            NSLog(@"JSON error!");
        }
        
    }else {
        // Catch all error
        NSLog(@"Error!");
    }
    
    return nil;

}


@end
