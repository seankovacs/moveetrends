//
//  ImageDownloader.h
//  moveetrends
//
//  Created by Sean Kovacs on 9/14/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownloader : NSObject

+ (void)downloadImage:(NSString *)urlString completion:(void (^)(UIImage *image))imageBlock;

@end
