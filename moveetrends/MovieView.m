//
//  MovieView.m
//  moveetrends
//
//  Created by Sean Kovacs on 9/14/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import "MovieView.h"
#import "ImageDownloader.h"

@implementation MovieView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.posterImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
        self.posterImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self addSubview:self.posterImageView];
    }
    return self;
}

- (void)loadMovie:(Movie *)movie
{
    [ImageDownloader downloadImage:movie.image completion:^(UIImage *image) {
        if(image) {
            self.posterImageView.image = image;
        }
    }];    
}

- (void)dealloc
{
//    NSLog(@"Removing view");
}

@end

