//
//  InfoViewController.h
//  moveetrends
//
//  Created by Sean Kovacs on 9/16/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface InfoViewController : UIViewController

@property (nonatomic,weak) Movie *movie;
@property (nonatomic,weak) IBOutlet UILabel *titleLabel;
@property (nonatomic,weak) IBOutlet UILabel *taglineLabel;
@property (nonatomic,weak) IBOutlet UITextView *descriptionTextView;
@property (nonatomic,weak) IBOutlet UILabel *ratingsLabel;
@property (nonatomic,weak) IBOutlet UILabel *genresLabel;
@property (nonatomic,weak) IBOutlet UIButton *trailerButton;
@property (nonatomic,weak) IBOutlet UIButton *facebookButton;
@property (nonatomic,weak) IBOutlet UIButton *twitterButton;
@property (nonatomic,weak) IBOutlet UIButton *closeButton;

- (void)loadMovie:(Movie *)movie;

- (IBAction)trailerTrash:(id)sender;
- (IBAction)close:(id)sender;
- (IBAction)facebook:(id)sender;
- (IBAction)twitter:(id)sender;

@end
