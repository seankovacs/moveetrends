//
//  MovieView.h
//  moveetrends
//
//  Created by Sean Kovacs on 9/14/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"

@interface MovieView : UIView
@property (nonatomic,strong) UIImageView *posterImageView;

- (void)loadMovie:(Movie *)movie;

@end
