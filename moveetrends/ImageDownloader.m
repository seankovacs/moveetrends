//
//  ImageDownloader.m
//  moveetrends
//
//  Created by Sean Kovacs on 9/14/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import "ImageDownloader.h"

const NSMutableDictionary *assetCache = nil;

@implementation ImageDownloader

+ (void)initialize
{
    if(!assetCache) assetCache = [NSMutableDictionary dictionary];
}

+ (void)downloadImage:(NSString *)urlString completion:(void (^)(UIImage *image))imageBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        if([assetCache valueForKey:urlString]) {
            UIImage *image = [UIImage imageWithData:[assetCache valueForKey:urlString]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                imageBlock(image);
            });
            
            return;
        }
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];

        [assetCache setValue:imageData forKey:urlString];
        
        UIImage *image = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            imageBlock(image);
        });

    });
}

@end
