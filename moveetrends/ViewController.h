//
//  ViewController.h
//  moveetrends
//
//  Created by Sean Kovacs on 9/14/13.
//  Copyright (c) 2013 SK DEV Solutions LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MovieView;

@interface ViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic,weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityView;


- (IBAction)refresh:(id)sender;

@end
